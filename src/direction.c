#include "../includes/shell.h"
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

int	g_pid = 0;

int	builtin_exec(t_command *command, t_node *node, int i)
{
	int	status;

	status = 0;
	if (!ft_strcmp(command->data, "echo"))
		status = (echo(command->arg));
	else if (!ft_strcmp(command->data, "cd"))
		status = (cd(command->arg));
	else if (!ft_strcmp(command->data, "pwd"))
		status = (pwd());
	else if (!ft_strcmp(command->data, "export"))
		status = (export(command->arg, node));
	else if (!ft_strcmp(command->data, "unset"))
		status = (unset(command->arg, node));
	else if (!ft_strcmp(command->data, "env"))
		status = (env(node->env));
	else if (!ft_strcmp(command->data, "exit"))
		status = (exit_command(command->arg));
	if ((node->right && ft_strcmp(command->data,
				node->right->data)) || node->next)
	{
		if (command->input_file)
			close(command->input_file);
		if (command->last_command)
			return (status);
		if (node->prev_fd != 0)
			close(node->prev_fd);
		if (node->fd && ((node->next && i == 1)
				|| (!node->right && node->next)))
			node->next->prev_fd = node->fd[READ_NO];
		else if (!i && node->fd)
			node->prev_fd = node->fd[READ_NO];
	}
	return (status);
}

void	wait_fork(t_node *node, t_command *command, int child_pid, int i)
{
	int	status;

	g_pid = child_pid;
	waitpid(child_pid, &status, 0);
	g_pid = 0;
	fork_exit(node, status);
	if (command->input_file)
		close(command->input_file);
	if (command->last_command)
		return ;
	if (node->prev_fd != 0)
		close(node->prev_fd);
	if (node->fd && node->next)
		node->next->prev_fd = node->fd[READ_NO];
	else if (!i && node->fd)
		node->prev_fd = node->fd[READ_NO];
}

void	send_command(t_node *node, t_command *command, int i)
{
	pid_t	child_pid;
	int		saved;

	if (!command || command->data == OUTPUT)
	{
		if (command)
			exit_message(command->data, "Command not found\n", 1);
		return ;
	}
	saved = dup(STDOUT_FILENO);
	fd_to_command(node, command);
	if (command && is_builtin(command->data))
		assign_status(node, builtin_exec(command, node, i));
	if (!is_builtin(command->data))
	{
		child_pid = fork();
		if (child_pid == 0)
		{
			signal(SIGINT, SIG_DFL);
			execute_command(command, node->last_status, node->env);
			exit(0);
		}
		else
		{
			wait_fork(node, command, child_pid, i);
			dup2(saved, STDOUT_FILENO);
			return ;
		}
	}
	dup2(saved, STDOUT_FILENO);
	free(node->fd);
}

void	write_file(t_node *node, t_command *command)
{
	int		fd;
	int		res;
	char	readbuffer[100];

	if (command->arg)
	{
		if (command->arg[1] != NULL)
			exit_message(NULL, "Error - Invalid usage.\n", 0);
	}
	fd = open(command->data, O_RDWR | O_CREAT | O_TRUNC, 0644);
	res = read(node->prev_fd, readbuffer, 100);
	while (res > 0)
	{
		write(fd, readbuffer, res);
		res = read(node->prev_fd, readbuffer, 100);
	}
	close(fd);
	if (node->prev_fd != 0)
		close(node->prev_fd);
	exit(EXIT_SUCCESS);
}

void	add_text(t_node *node, t_command *command)
{
	char	readbuffer[100];
	int		fd;
	int		res;

	if (command->arg)
	{
		if (command->arg[1] != NULL)
			exit_message(NULL, "Error - Invalid usage.\n", 0);
	}
	fd = open(command->data, O_RDWR | O_APPEND | O_CREAT, 0644);
	res = read(node->prev_fd, readbuffer, 100);
	while (res > 0)
	{
		write(fd, readbuffer, res);
		res = read(node->prev_fd, readbuffer, 100);
	}
	close(fd);
	fd = open(command->data, O_RDWR | O_APPEND | O_CREAT, 0644);
	if (node->next && ft_strcmp(node->next->operator, ">") == 0)
		node->next->prev_fd = fd;
	if (node->prev_fd != 0)
		close(node->prev_fd);
	exit(EXIT_SUCCESS);
}
