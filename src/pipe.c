#include "../includes/shell.h"
#include <unistd.h>

int	pipe_execute(t_node *node)
{
	int			i;
	int			*array;
	t_command	*command;

	i = 0;
	array = (int *) malloc(sizeof(int *) * 2);
	node->fd = array;
	while (i < 2)
	{
		pipe(node->fd);
		if (!i)
			command = node->left;
		else if (i)
			command = node->right;
		if (command == NULL)
			break ;
		send_command(node, command, i);
		i++;
	}
	return (0);
}
