#include "../includes/shell.h"

char	*ft_getenv(char **envp, char *key)
{
	int				i;
	char			*temp;

	i = 0;
	while (envp[i])
	{
		temp = ft_strnstr(envp[i], key, ft_strlen(key));
		if (temp)
			return (ft_strdup(&temp[ft_strlen(key) + 1]));
		i++;
	}
	return (NULL);
}

char	**update_environment_variable(char **envp, char *key, char *value)
{
	int		i;
	char	*new_variable;
	char	**new_envp;

	new_variable = ft_strdup(key);
	new_variable = ft_strjoin(new_variable, "=");
	new_variable = ft_strjoin(new_variable, value);
	i = 0;
	while (envp[i])
	{
		if (ft_strncmp(envp[i], key, ft_strlen(key)) == 0)
			return (envp[i] = new_variable, envp);
		i++;
	}
	new_envp = (char **) malloc((i + 2) * sizeof(char *));
	if (new_envp == NULL)
		return (free(new_variable), NULL);
	new_envp = copy_array(envp, i + 1);
	new_envp[i] = new_variable;
	new_envp[i + 1] = NULL;
	return (free_array(envp), new_envp);
}

int	control_expression(char *expression)
{
	if ((expression[0] >= 'A' && expression[0] <= 'Z')
		|| (expression[0] >= 'a' && expression[0] <= 'z')
		|| (expression[0] == '_'))
		return (1);
	else
		return (0);
}

char	*ft_trim(char *str, char c)
{
	char	*res;
	int		i;

	i = 0;
	while (str[i] && str[i] != c)
		i++;
	res = (char *) malloc(i + 1);
	i = 0;
	while (str[i] && str[i] != c)
	{
		res[i] = str[i];
		i++;
	}
	res[i] = 0;
	return (res);
}

int	export(char **args, t_node *node)
{
	int		i;
	char	*key;
	char	**envp;

	i = 1;
	envp = node->env;
	while (args[i])
	{
		if (!control_expression(args[i]))
		{
			update_env(node, envp);
			exit_message(NULL, "Error - Invalid usage.\n", 1);
			return (EXIT_FAILURE);
		}
		if (ft_strchr(args[i], '='))
		{
			key = ft_trim(args[i], '=');
			envp = update_environment_variable(envp, key,
					ft_strchr(args[i], '=') + 1);
			free(key);
		}
		i++;
	}
	update_env(node, envp);
	return (EXIT_SUCCESS);
}
