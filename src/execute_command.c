#include "../includes/shell.h"
#include <unistd.h>

int	execute_command(t_command *command, int last_status, char **envp)
{
	assign_env(command->arg, last_status);
	execve(get_location(command->arg[0], envp), command->arg, envp);
	exit_message(command->arg[0], "Execution failed\n", 0);
	exit(EXIT_FAILURE);
}
