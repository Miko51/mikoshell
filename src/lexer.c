#include "../includes/shell.h"

char	*delete_quote(t_command *current_command, char *token)
{
	char	*temp;

	if (token[0] != 39)
		current_command->quotes = 1;
	if (token[0] == 39 || token[0] == '"')
	{
		temp = token;
		token = ft_strdup(&token[1]);
		free(temp);
	}
	return (token);
}

t_command	*add_token(t_node *node, t_command *current_command,
						char *token, int *arg_flag)
{
	if (*arg_flag == 1)
		current_command->arg = remake_arguments(current_command->arg,
				token);
	else
	{
		if (*arg_flag == -1)
			current_command = init_leaf(node, token, 1);
		else
			current_command = init_leaf(node, token, 0);
		current_command->arg = remake_arguments(NULL, token);
		*arg_flag = 1;
	}
	return (current_command);
}

t_node	*add_node(t_node *node, int *arg_flag, char *token)
{
	if (!node->operator)
	{
		node->operator = ft_strdup(token);
		free(token);
	}
	else
		node = create_node(node, token);
	*arg_flag = 0;
	return (node);
}

t_node	*first_cleaning(t_node *node, int *arg_flag, char *str)
{
	char		*token;
	t_command	*command;

	command = NULL;
	token = pre_trim(str, " ", NULL, node);
	if (!ft_strcmp(token, "|"))
	{
		free(token);
		exit_message(NULL, "Error - Invalid Syntax.", 1);
		return (NULL);
	}
	while (find_operator(token))
	{
		*arg_flag = -1;
		node = add_node(node, arg_flag, token);
		token = pre_trim(str, " ", NULL, node);
		*arg_flag = -1;
		command = add_token(node, command, token, arg_flag);
		token = pre_trim(str, " ", NULL, node);
	}
	*arg_flag = 0;
	if (token)
		command = add_token(node, command, token, arg_flag);
	*arg_flag = 1;
	return (node);
}

t_node	*lexer(char *str, t_node *node)
{
	char		*token;
	t_command	*command;
	t_node		*root;
	int			arg_flag;
	int			res;

	root = node;
	command = NULL;
	arg_flag = 0;
	node = first_cleaning(node, &arg_flag, str);
//	print_new_node(root);
	if (!node)
		return (NULL);
	if (node->right)
		command = node->right;
	else
		command = node->left;
	while (1)
	{
//		print_new_node(root);
//		printf("\n");
		token = pre_trim(str, " ", command, node);
		if (!token)
			break ;
		res = find_operator(token);
		if (token && res != 0)
			node = add_node(node, &arg_flag, token);
		else if (token)
			command = add_token(node, command, token, &arg_flag);
	}
//	printf("***********\n");
//	print_new_node(root);
	return (root);
}
