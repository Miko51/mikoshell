#include "../includes/shell.h"
#include <stdio.h>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>

extern int	g_pid;

int	find_mark(char c)
{
	if (!c)
		return (0);
	if (c == '>')
		return (1);
	else if (c == '<')
		return (2);
	else if (c == '|')
		return (3);
	return (0);
}

char	*filter_input(char *str)
{
	int	i;
	int	res;
	int	res2;

	i = 0;
	while (str[i])
	{
		res = find_mark(str[i]);
		res2 = find_mark(str[i + 1]);
		if (res == res2 && res)
		{
			str = add_spaces(str, i);
			if (str[i] == ' ')
				i++;
			while (str[i] != ' ')
				i++;
		}
		else if (res > 0 && res2 > 0)
			return (exit_message(NULL, "Invalid syntax error", 1), NULL);
		else if (res && !res2)
			str = add_space(str, i);
		i++;
	}
	return (str);
}

void	final_state(t_node *node)
{
	free_array(node->env);
	free(node);
	rl_clear_history();
}

t_node	*new_prompt(t_node *node, int status, char **envp)
{
	node_free(node);
	node = init_root(envp);
	node->last_status = status;
	return (node);
}

char	*get_input(t_node *node)
{
	char	*buf;
	char	*prompt;
	int		status;
	int		saved_stdin;

	status = 0;
	saved_stdin = dup(STDIN_FILENO);
	while (1)
	{
		prompt = input_str();
		buf = readline(prompt);
		if (ft_strlen(buf) > 0)
		{
			add_history(buf);
			run_command(node, &(node->env), &status, buf);
			node = new_prompt(node, status, node->env);
			dup2(saved_stdin, STDIN_FILENO);
		}
		else if (!buf)
			exit(-1);
		g_pid = 0;
		free(prompt);
	}
	return (final_state(node), buf);
}
