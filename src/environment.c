#include "../includes/shell.h"

char	*get_env(char *env, int status, char **envp)
{
	char	*path;
	char	*copy_path;

	if (!ft_strcmp(env, "?"))
	{
		free(env);
		return (ft_itoa(status));
	}
	path = ft_getenv(envp, env);
	if (!path)
		return (NULL);
	copy_path = ft_strdup(path);
	free(env);
	return (copy_path);
}

char	*special_chars(char *env, int status, char **envp)
{
	int		i;
	int		j;
	char	*temp;

	i = 0;
	j = 0;
	while ((env[i] == '?' && i == 0)
		|| (env[i] && !is_special(env[i])))
		i++;
	temp = (char *) malloc(sizeof(char) * (i + 1));
	while (j < i)
	{
		temp[j] = env[j];
		j++;
	}
	temp[j] = 0;
	temp = get_env(temp, status, envp);
	temp = ft_strjoin(temp, &env[i]);
	return (temp);
}

char	*cut_quotes(char *token)
{
	int	i;

	i = 0;
	if (!token)
		return (NULL);
	while (token[i] && token[i] != '"')
		i++;
	return (ft_trim(token, token[i]));
}

#include <stdio.h>
char	*cut_dollar(char *token, int *flag)
{
	char	*res;

	res = NULL;
	if (!token)
		return (NULL);
	*flag = 0;
	if (token[0] == '$')
		*flag = 1;
	res = ft_strtok(token, "$");
	if (!res && token[0] == '$' && !token[1])
	{
		if (!token[1])
			token[0] = 0;
		res = ft_strdup("$");
		*flag = 0;
	}
	return (res);
}

char	*filter_env(t_command *current_command, char *token, t_node *node)
{
	char	*res;
	char	*temp;
	char	*str;
	int		flag;

	temp = NULL;
	str = NULL;
	token = cut_quotes(token);
	res = cut_dollar(token, &flag);
	while (token && current_command->quotes && res)
	{
		if (flag && (!res || !*res || (is_special(res[0]) && res[0] != '?')))
			temp = res;
		else if (res && flag)
			temp = special_chars(res, node->last_status, node->env);
		if (flag && temp)
			str = ft_strjoin(str, temp);
		if (!flag && res)
			str = ft_strjoin(str, res);
		res = cut_dollar(token, &flag);
	}
	current_command->quotes = 0;
	return (str);
}
