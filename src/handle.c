#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <readline/readline.h>

extern int	g_pid;

void	kill_input(int signal)
{
	(void)signal;
	if (!g_pid)
	{
		rl_on_new_line();
		rl_replace_line("", 0);
		ioctl(STDIN_FILENO, TIOCSTI, "\n");
		return ;
	}
	else
		kill(g_pid, SIGINT);
}
/*
void	kill_signal(int signal)
{
	(void)signal;
	exit(128 + 2);
}
*/
