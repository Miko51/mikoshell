#include "../includes/shell.h"
#include <limits.h>

#include <stdio.h>
long long int	ft_latoi(const char *str)
{
	unsigned long long	nbr;
	int					isneg;
	int					i;

	i = 0;
	nbr = 0;
	isneg = 1;
	while (str[i] != '\0' && (str[i] == 32
			|| str[i] == '\t' || str[i] == '\n'
			|| str[i] == '\r' || str[i] == '\v' || str[i] == '\f'))
		i++;
	if (str[i] != '\0' && (str[i] == '+' || str[i] == '-'))
	{
		if (str[i] == '-')
			isneg = -1;
		i++;
	}
	while (str[i] != '\0' && ((isneg == -1 && nbr < (unsigned long long)
				LONG_MIN) || (isneg == 1 && nbr
				< (unsigned long long) LONG_MAX)))
		nbr = (nbr * 10) + (str[i++] - '0');
	if ((isneg == -1 && nbr > (unsigned long long) LONG_MIN)
		|| (isneg == 1 && nbr > (unsigned long long) LONG_MAX))
		exit_message(NULL, "Error numeric argument required\n", 0);
	return ((long long int)(nbr * isneg));
}

int	ft_isnum(char *str)
{
	if (!*str)
		return (0);
	if (*str == '-' || *str == '+')
		str++;
	while (*str)
		if (!ft_isdigit(*str++))
			return (0);
	return (1);
}

int	exit_arg_control(long long int value)
{
	if (value < LONG_MIN || value > LONG_MAX)
	{
		exit_message(NULL, "Error numeric argument required\n", 1);
		exit (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

int	exit_command(char **args)
{
	long long int	value;

	printf("exit\n");
	if (args[1])
	{
		if (!ft_isnum(args[1]))
			exit_message(NULL, "Error numeric argument required\n", 0);
		if (args[2])
			return (exit_message(NULL, "Too many arguments\n", 1));
		else
		{
			value = ft_latoi(args[1]);
			exit_arg_control(value);
			exit(value);
		}
	}
	exit(EXIT_FAILURE);
}
