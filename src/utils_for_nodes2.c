#include "../includes/shell.h"

void	assign_env(char **arg, int last_status)
{
	int		i;

	if (!arg)
		return ;
	i = 0;
	while (arg[i])
	{
		if (arg[i][0] == '$' && arg[i][1] == '?')
		{
			arg[i] = ft_itoa(last_status);
			break ;
		}
		i++;
	}
}

void	assign_status(t_node *node, int status)
{
	if (!node)
		return ;
	while (node->prev)
		node = node->prev;
	while (node)
	{
		node->last_status = status;
		node = node->next;
	}
}

void	update_env(t_node *node, char **env)
{
	if (!node)
		return ;
	while (node->prev)
		node = node->prev;
	while (node)
	{
		node->env = env;
		node = node->next;
	}
}

char	*quotes_trim(char *str)
{
	int		i;
	int		j;
	char	*res;
	char	*copy_str;
	char	quote;

	i = 0;
	j = -1;
	while (str[i] == ' ')
		i++;
	copy_str = ft_strdup(&str[i]);
	i = 0;
	quote = copy_str[i++];
	while (copy_str[i] && copy_str[i] != quote)
		i++;
	if (copy_str[i] == quote)
		i++;
	res = (char *) malloc(sizeof(char) * (i));
	while (i > ++j)
		res[j] = copy_str[j];
	res[j] = 0;
	if (copy_str[i])
		return (ft_strcpy(str, &copy_str[i]), free(copy_str), res);
	else
		return (str[0] = '\0', free(copy_str), res);
}
