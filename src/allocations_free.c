#include "../includes/shell.h"

void	free_array(char **str)
{
	int	i;

	i = 0;
	if (!str)
		return ;
	while (str[i])
	{
		free(str[i]);
		i++;
	}
	free(str);
}

void	free_command(t_node *node)
{
	t_command	*command;
	int			i;

	i = 0;
	while (i < 2)
	{
		if (!i)
			command = node->left;
		else
			command = node->right;
		if (!command)
			break ;
		free(command->data);
		free_array(command->arg);
		free(command);
		i++;
	}
}

void	node_free(t_node *node)
{
	t_node	*next;

	while (node)
	{
		next = node->next;
		free_command(node);
		if (node->operator)
			free(node->operator);
		free(node);
		node = next;
	}
}
