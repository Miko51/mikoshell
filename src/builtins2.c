#include "../includes/shell.h"
#include <stdio.h>
#include <unistd.h>

char	*get_working_dir(void)
{
	char	*dir;

	dir = getcwd(NULL, 0);
	if (dir == NULL)
		exit_message(NULL, "Allocation error \n", 1);
	return (dir);
}

int	pwd(void)
{
	char	*dir;

	dir = get_working_dir();
	if (dir)
	{
		printf("%s\n", dir);
		free(dir);
		return (EXIT_SUCCESS);
	}
	return (EXIT_FAILURE);
}
