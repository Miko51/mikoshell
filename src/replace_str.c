#include "../includes/shell.h"
#include <stdio.h>

char	*add_space(char *str, int i)
{
	int	length;

	length = ft_strlen(str);
	if (find_mark(str[i]) && (i + 1) < length && str[i + 1] != ' ')
		str = replace_str(str, i + 1);
	if (find_mark(str[i]) && (i - 1) >= 0 && str[i - 1] != ' ')
	{
		str = replace_str(str, i);
	}
	return (str);
}

char	*add_spaces(char *str, int i)
{
	int	length;

	length = ft_strlen(str);
	if ((i - 1) >= 0 && str[i - 1] != ' ')
	{
		str = replace_str(str, i);
		i++;
	}
	if ((i + 2) < length && str[i + 2] != ' ')
	{
		str = replace_str(str, i + 2);
		i++;
	}
	return (str);
}

char	*replace_str(char *str, int index)
{
	char	*new_str;
	int		i;
	int		j;

	new_str = (char *) malloc(sizeof(char) * (ft_strlen(str) + 2));
	if (!new_str)
		return (NULL);
	i = 0;
	j = 0;
	while (str[i])
	{
		if (index == i && find_mark(str[i]) && str[i - 2] == ' ')
		{
			new_str[j - 1] = str[i - 2];
			new_str[j] = str[i - 1];
			new_str[j++] = str[i++];
		}
		else if (index == i && find_mark(str[i]) && str[i + 1] == ' ')
		{
			new_str[j++] = str[i++];
			str[i] = ' ';
			str[i + 1] = '"';
		}
		else if (index == i)
		{
			new_str[j++] = ' ';
			new_str[j++] = str[i++];
		}
		else
			new_str[j++] = str[i++];
	}
	return (free(str), new_str[j] = 0, new_str);
}
