[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# mikoshell 
**Mikoshell özel olarak geliştirdiğim bir shelldir. Basit shell özelliklerine sahiptir.**

## Kullanım
```bash
git clone https://gitlab.com/Miko51/mikoshell && cd mikoshell 
make && ./shell
```

Hatalar alabilirsiniz, testleri henüz tamamlanmadı <br />
