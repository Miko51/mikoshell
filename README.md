[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 

# mikoshell 
**Mikoshell is a custom shell I developed. It has basic shell properties.**

## Usage 
```bash
git clone https://gitlab.com/Miko51/mikoshell && cd mikoshell 
make && ./shell
```

You could encounter errors, tests have not finished yet. <br />
